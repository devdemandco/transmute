var path          = require('path'),
    webpack       = require('webpack'),
    htmlWebpack   = require('html-webpack-plugin'),
    BUILD_DIR     = path.resolve(__dirname, 'dist'),
    APP_DIR       = path.resolve(__dirname, 'src'),
    PORT          = process.env.PORT || "8002",
    HOST          = process.env.HOST || "localhost",
    sassDirs      = [APP_DIR + '/styles/'],
    reactDirs     = [APP_DIR];

// Export the webpack configuration
module.exports = {

  // Enable source maps as a devtool
  devtool: 'source-map',

  // Setup the entry point for the react application
  entry: [
    `webpack-dev-server/client?http://${HOST}:${PORT}`,
    'webpack/hot/dev-server',
    APP_DIR + '/components/app.jsx',
  ],

  // Find files with the following extensions
  resolve: {
    root: [].concat(sassDirs, reactDirs),
    extensions: ['', '.js', '.jsx'],
  },

  // Setup where we want the resulting vanilla js to go
  output: {
    path: BUILD_DIR,
    file: 'bundle.js',
    publicPath: '/'
  },

  // List plugins we use to compile and run our app
  module: {
    loaders: [
      // Compile all jsx files to vanilla js using babel
      { test: /\.jsx?/, include: APP_DIR, loader: 'babel' },
      // Minify and include all css files that have been imported
      { test: /\.css?/, loader: "style-loader!css-loader?sourceMap" },
      // Compile and minify all sass files
      { test: /\.sass?/, loader: "style-loader!css-loader?sourceMap,modules!sass-loader?sourceMap" },
    ]
  },

  // Sass loader specific config
  sassLoader: {
    // Setup a easier route to sass files
    includePaths: [
      APP_DIR + '/sass/'
    ]
  },

  // Configuration for the dev server started by `npm start`
  devServer: {
    contentBase: BUILD_DIR, // Location of built files
    hot: true, // Turn on hot-reload
    port: PORT, // Use the port specified at runtime
    historyApiFallback: true, // Let the react app handle 404s
  },

  // Add plugins to webpack
  plugins: [
    new webpack.NoErrorsPlugin(), // Allows compilation to continue for other modules even if one fails
    new webpack.HotModuleReplacementPlugin(),
    new htmlWebpack({ // Creates a dynamic html that includes css js etc on demand when added to jsx files
      template: APP_DIR + "/partials/template.html"
    })
  ]
};
