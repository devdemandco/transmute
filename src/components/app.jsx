import React from 'react'
import ReactDOM from 'react-dom'

import Anchor from 'components/Anchor'

ReactDOM.render(
  <Anchor href="https://google.com">Haha</Anchor>,
  document.getElementById('app')
)
